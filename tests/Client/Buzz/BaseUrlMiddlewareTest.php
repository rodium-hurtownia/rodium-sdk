<?php

namespace Rodium\Sdk\Client\Buzz;

use Nyholm\Psr7\Uri;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class BaseUrlMiddlewareTest extends TestCase
{
    private const BASE_URL = "https://some-base-url.com";

    /** @var BaseUrlMiddleware */
    private $middleware;

    protected function setUp(): void
    {
        $this->middleware = new BaseUrlMiddleware(self::BASE_URL);
    }

    /**
     * @test
     */
    public function itAddsBaseUrlToGivenUri()
    {
        /** @var RequestInterface|ObjectProphecy $request */
        $request = $this->prophesize(RequestInterface::class);
        $modifiedRequest = $this->prophesize(RequestInterface::class)->reveal();
        $response = $this->prophesize(ResponseInterface::class)->reveal();

        $uri = new Uri('/some-api-call');
        $request->getUri()->willReturn($uri);
        $request->withUri(new Uri(self::BASE_URL . $uri))->willReturn($modifiedRequest);
        $next = function (RequestInterface $request) use ($modifiedRequest, $response) {
            $this->assertSame($modifiedRequest, $request);
            return $response;
        };

        $this->assertSame(
            $response,
            $this->middleware->handleRequest($request->reveal(), $next)
        );
    }

    /**
     * @test
     */
    public function itPassesTheResponseFilter()
    {
        /** @var RequestInterface $request */
        $originalRequest = $this->prophesize(RequestInterface::class)->reveal();
        $originalResponse = $this->prophesize(ResponseInterface::class)->reveal();

        $next = function(RequestInterface $request, $response) use ($originalRequest, $originalResponse) {
            $this->assertSame($originalRequest, $request);
            $this->assertSame($originalResponse, $response);

            return $response;
        };

        $this->assertSame(
            $originalResponse,
            $this->middleware->handleResponse($originalRequest, $originalResponse, $next)
        );
    }
}