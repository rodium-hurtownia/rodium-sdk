<?php

namespace Rodium\Sdk\Client\Buzz;

use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rodium\Sdk\Client\ApiKey;

class ApiKeyAuthMiddlewareTest extends TestCase
{
    private const API_KEY = "the-random-key";

    /** @var ApiKeyAuthMiddleware */
    private $middleware;

    protected function setUp(): void
    {
        $this->middleware = new ApiKeyAuthMiddleware(new ApiKey(self::API_KEY));
    }

    /**
     * @test
     */
    public function itAddsTheRodiumApiKeyHeaderToTheRequest()
    {
        /** @var RequestInterface|ObjectProphecy $request */
        $request = $this->prophesize(RequestInterface::class);
        $response = $this->prophesize(ResponseInterface::class)->reveal();

        $modifiedRequest = $this->prophesize(RequestInterface::class)->reveal();
        $request->withHeader(ApiKey::HEADER, self::API_KEY)->willReturn($modifiedRequest);

        $next = function (RequestInterface $request) use ($modifiedRequest, $response) {
            $this->assertSame($modifiedRequest, $request);
            return $response;
        };

        $this->assertSame($response, $this->middleware->handleRequest($request->reveal(), $next));
    }

    /**
     * @test
     */
    public function itPassesTheResponseFilter()
    {
        /** @var RequestInterface $request */
        $originalRequest = $this->prophesize(RequestInterface::class)->reveal();
        $originalResponse = $this->prophesize(ResponseInterface::class)->reveal();

        $next = function(RequestInterface $request, $response) use ($originalRequest, $originalResponse) {
            $this->assertSame($originalRequest, $request);
            $this->assertSame($originalResponse, $response);

            return $response;
        };

        $this->assertSame(
            $originalResponse,
            $this->middleware->handleResponse($originalRequest, $originalResponse, $next)
        );
    }
}
