<?php

namespace Rodium\Sdk\Client;

use Buzz\Browser;
use PHPUnit\Framework\TestCase;
use Rodium\Sdk\Catalog\Categories\CategoriesHttpApi;
use Rodium\Sdk\Catalog\Items\ItemsHttpApi;
use Rodium\Sdk\Order\OrdersHttpApi;

class ApiFactoryTest extends TestCase
{
    /** @var ApiFactory */
    private $apiFactory;

    protected function setUp(): void
    {
        $this->apiFactory = new ApiFactory(
            "http://base-url",
            new ApiKey("api-key")
        );
    }

    /**
     * @test
     */
    public function itCreatesCategoriesApi()
    {
        $this->assertInstanceOf(CategoriesHttpApi::class, $this->apiFactory->categoriesApi());
    }

    /**
     * @test
     */
    public function itCreatesItemsApi()
    {
        $this->assertInstanceOf(ItemsHttpApi::class, $this->apiFactory->itemsApi());
    }

    /**
     * @test
     */
    public function itCreatesOrdersApi()
    {
        $this->assertInstanceOf(OrdersHttpApi::class, $this->apiFactory->ordersApi());
    }
}