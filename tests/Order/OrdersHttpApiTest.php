<?php

namespace Rodium\Sdk\Order;

use Rodium\Sdk\AbstractIntegrationTest;
use Rodium\Sdk\Client\AbstractApiException;
use Rodium\Sdk\Error\ValidationError;
use Rodium\Sdk\Page\Page;

class OrdersHttpApiTest extends AbstractIntegrationTest
{
    /** @var OrdersApi */
    private $ordersHttpApi;

    protected function setUp(): void
    {
        $branchId = (int)getenv("rodium.orders.branch_id") ?: null;
        $this->ordersHttpApi = $this->apiFactory()->ordersApi($branchId);
    }

    /**
     * @test
     */
    public function itGetsOrderById()
    {
        $orderId = getenv("rodium.orders.order_id");
        if (!$orderId) {
            $this->markTestSkipped("\"rodium.orders.order_id\" must be set.");
            return;
        }

        $order = $this->ordersHttpApi->orderOfId($orderId);
        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals($orderId, $order->id());
    }

    /**
     * @test
     */
    public function itGetsOrders()
    {
        $orders = $this->ordersHttpApi->orders();
        $this->assertInstanceOf(Page::class, $orders);
        $this->assertNotEmpty($orders->elements());
        $this->assertInstanceOf(Order::class, $orders->elements()[0]);
    }

    /**
     * @test
     */
    public function itPlacesTheOrder()
    {
        $variantId = getenv("rodium.orders.variant_id");
        if (!$variantId) {
            $this->markTestSkipped("rodium.orders.variant_id must be set.");
            return;
        }

        $order = Order::newOrder()
            ->withVariant($variantId, 1)
            ->withNotes("Test order via API. Please cancel.");

        $placedOrder = $this->ordersHttpApi->place($order);
        $this->assertInstanceOf(Order::class, $placedOrder);
        $this->assertNotEmpty($placedOrder->id());
    }
}
