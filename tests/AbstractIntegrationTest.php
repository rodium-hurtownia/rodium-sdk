<?php

namespace Rodium\Sdk;

use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use Rodium\Sdk\Client\ApiFactory;
use Rodium\Sdk\Client\ApiKey;

abstract class AbstractIntegrationTest extends TestCase
{
    /** @var Generator */
    private $faker;

    protected function apiFactory(): ApiFactory
    {
        $baseUrl = $this->baseUrl();
        $apiKey = $this->apiKey();

        $curlOptions = [];
        $disableSslChecks = (bool)getenv("ssl_checks.disabled");
        if ($disableSslChecks) {
            $curlOptions['verify'] = false;
        }

        return new ApiFactory($baseUrl, $apiKey, $curlOptions);
    }

    protected function faker(): Generator
    {
        if (!$this->faker) {
            $this->faker = Factory::create('pl_PL');
        }

        return $this->faker;
    }

    private function apiKey(): ?ApiKey
    {
        $apiKey = getenv('rodium.api_key');
        if (!$apiKey) {
            $this->markTestSkipped("'rodium.api_key' env variable is not set");
        }

        return new ApiKey($apiKey);
    }

    private function baseUrl(): ?string
    {
        $baseUrl = getenv('rodium.base_url');
        if (!$baseUrl) {
            $this->markTestSkipped("'rodium.base_url' env variable is not set");
        }

        return $baseUrl;
    }
}