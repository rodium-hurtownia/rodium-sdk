<?php

namespace Rodium\Sdk\Catalog\Categories;

use Rodium\Sdk\AbstractIntegrationTest;

class CategoriesHttpApiTest extends AbstractIntegrationTest
{
    /** @var CategoriesHttpApi */
    private $categoriesApi;

    protected function setUp(): void
    {
        $this->categoriesApi = $this->apiFactory()->categoriesApi();
    }

    /**
     * @test
     */
    public function itGetsCategoryById()
    {
        $category = $this->categoriesApi->categoryOfId($this->categoryId());

        $this->assertInstanceOf(Category::class, $category);
        $this->assertEquals($this->categoryId(), $category->id());
    }

    /**
     * @test
     */
    public function itReturnsNullOnNonExistingCategory()
    {
        $category = $this->categoriesApi->categoryOfId(-1234);

        $this->assertNull($category);
    }

    /**
     * @test
     */
    public function itGetsAllCategories()
    {
        $categories = $this->categoriesApi->all();
        $this->assertIsArray($categories);
        $this->assertNotEmpty($categories);
    }

    /**
     * @test
     */
    public function itGetsChildrenOfTheCategory()
    {
        $children = $this->categoriesApi->children($this->categoryId());
        $this->assertIsArray($children);
        $this->assertNotEmpty($children);
    }

    /**
     * @test
     */
    public function itGetsTreeOfTheCategory()
    {
        $category = $this->categoriesApi->tree($this->categoryId());

        $this->assertInstanceOf(Category::class, $category);
        $this->assertEquals(2, $category->id());
        $this->assertNotEmpty($category->children());
    }

    private function categoryId(): int
    {
        $id = getenv("rodium.categories.category_id");
        if ($id == null) {
            $this->markTestSkipped("\"rodium.categories.category_id\" must be set.");
        }

        return (int)$id;
    }
}
