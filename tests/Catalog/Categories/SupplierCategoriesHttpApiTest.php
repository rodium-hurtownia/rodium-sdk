<?php

namespace Rodium\Sdk\Catalog\Categories;

use Rodium\Sdk\AbstractIntegrationTest;

class SupplierCategoriesHttpApiTest extends AbstractIntegrationTest
{
    /** @var SupplierCategoriesApi */
    private $categoriesApi;

    protected function setUp(): void
    {
        $this->categoriesApi = $this->apiFactory()->supplierCategoriesApi();
    }

    /**
     * @test
     */
    public function itGetsCategoryTree(): void
    {
        $category = $this->categoriesApi->tree();

        $this->assertInstanceOf(Category::class, $category);
        $this->assertFalse($category->isHasSupplierAccess());
    }

    /**
     * @test
     */
    public function itGetsFlatCategoriesList(): void
    {
        $categories = $this->categoriesApi->all();

        $this->assertIsArray($categories);
        $this->assertGreaterThan(0, count($categories));
    }
}
