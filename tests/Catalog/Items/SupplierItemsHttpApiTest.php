<?php

namespace Rodium\Sdk\Catalog\Items;

use Rodium\Sdk\AbstractIntegrationTest;

class SupplierItemsHttpApiTest extends AbstractIntegrationTest
{
    /** @var SupplierItemsHttpApi */
    private $api;

    /** @var Item[] */
    private $createdItems = [];

    /** @var Variant[] */
    private $createdVariants = [];

    protected function setUp(): void
    {
        $this->api = $this->apiFactory()->supplierItemsApi();
    }

    protected function tearDown(): void
    {
        foreach ($this->createdItems as $item) {
            try {
                $this->api->removeItem($item->id());
            } catch (\Exception $e) {
            }
        }

        foreach ($this->createdVariants as $variant) {
            try {
                $this->api->removeVariant($variant->id());
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * @test
     */
    public function itCreatesItem(): void
    {
        $item = Item::forCategory(
            $this->categoryId(),
            "Brynant-".$this->faker()->numberBetween(1000, 9999)
        );
        $this->createdItems[] = $createdItem = $this->api->createItem($item);

        $this->assertNotNull($createdItem->id());
        $this->assertEquals($item->categoryId(), $createdItem->categoryId());
        $this->assertEquals($item->symbol(), $createdItem->symbol());
    }

    /**
     * @test
     */
    public function itRemovesItem(): void
    {
        $item = Item::forCategory($this->categoryId(), "Brynant-".$this->faker()->numberBetween(1000, 9999));
        $this->createdItems[] = $createdItem = $this->api->createItem($item);
        $this->api->removeItem($createdItem->id());

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function itCreatesSingleVariant(): void
    {
        $variant = $this->newSingleVariant($this->categoryId());

        $this->createdVariants[] = $createdVariant = $this->api->createVariant($variant);

        $this->assertNotNull($createdVariant->id());
        $this->assertNotNull($createdVariant->itemId());
    }

    /**
     * @test
     */
    public function itCreatesVariantForItem(): void
    {
        $this->createdVariants[] = $singleVariant = $this->api->createVariant(
             $this->newSingleVariant($this->categoryId())
        );

        $variant = $this->newVariantForItem($singleVariant->itemId());
        $this->createdVariants[] = $createdVariant = $this->api->createVariant($variant);

        $this->assertNotNull($createdVariant->id());
        $this->assertEquals($singleVariant->itemId(), $createdVariant->itemId());
    }

    /**
     * @test
     */
    public function itRemovesVariant(): void
    {
        $this->createdVariants[] = $singleVariant = $this->api->createVariant(
            $this->newSingleVariant($this->categoryId())
        );

        $this->api->removeVariant($singleVariant->id());
        $variants = $this->api->variantsOfItem($singleVariant->itemId());

        $this->assertEmpty($variants);
    }

    /**
     * @test
     */
    public function itGetsVariantsOfItem(): void
    {
        $allVariants = [];
        $this->createdVariants[] = $allVariants[] = $singleVariant = $this->api->createVariant(
            $this->newSingleVariant($this->categoryId())
        );
        $this->createdVariants[] = $allVariants[] = $this->api->createVariant($this->newVariantForItem($singleVariant->itemId()));
        $this->createdVariants[] = $allVariants[] = $this->api->createVariant($this->newVariantForItem($singleVariant->itemId()));

        $variantsOfItem = $this->api->variantsOfItem($singleVariant->itemId());
        $this->assertCount(count($allVariants), $variantsOfItem);
        foreach ($variantsOfItem as $variant) {
            $this->assertEquals($singleVariant->itemId(), $variant->itemId());
        }
    }

    /**
     * @test
     */
    public function itAddsPhotoToVariant()
    {
        $this->createdVariants[] = $variant = $this->api->createVariant($this->newSingleVariant());
        $this->api->addVariantPhoto($variant->id(), $this->diamondImage(1));
        $variantWithPhotos = $this->api->addVariantPhoto($variant->id(), $this->diamondImage(2));

        $this->assertCount(2, $variantWithPhotos->images());
    }

    /**
     * @test
     */
    public function itReplacesThePhotoOfGivenIndex()
    {
        $this->createdVariants[] = $variant = $this->api->createVariant($this->newSingleVariant());
        $this->api->addVariantPhoto($variant->id(), $this->diamondImage(1));
        $this->api->addVariantPhoto($variant->id(), $this->diamondImage(2));

        $variantWithPhotos = $this->api->variantOfId($variant->id());
        $variantPhotos = $variantWithPhotos->images();

        $variantWithUpdatedPhoto = $this->api->changeVariantPhoto($variant->id(), $this->diamondImage(4), 1);
        $variantUpdatedPhotos = $variantWithUpdatedPhoto->images();

        $this->assertCount(2, $variantUpdatedPhotos);
        $this->assertNotEquals($variantPhotos[1], $variantUpdatedPhotos[1]);
    }

    /**
     * @test
     */
    public function itRemovesThePhotoOfGivenIndex()
    {
        $this->createdVariants[] = $variant = $this->api->createVariant($this->newSingleVariant());
        $this->api->addVariantPhoto($variant->id(), $this->diamondImage(1));
        $this->api->addVariantPhoto($variant->id(), $this->diamondImage(2));

        $variantWithPhotos = $this->api->variantOfId($variant->id());
        $photoToBeRemoved = $variantWithPhotos->images()[1];

        $variantWithRemovedPhoto = $this->api->removeVariantPhoto($variant->id(), 1);

        $this->assertNotContains($photoToBeRemoved, $variantWithRemovedPhoto->images());
    }

    /**
     * @test
     */
    public function itUpdatesStock(): void
    {
        $this->createdVariants[] = $variant = $this->api->createVariant($this->newSingleVariant());
        $updatedVariant = $this->api->updateVariantStock($variant->id(), $newStock = $this->faker()->numberBetween(2, 10));

        $this->assertEquals($newStock, $updatedVariant->stockQuantity());
    }

    private function newVariantForItem(int $itemId): Variant
    {
        return Variant::forItem(
            $itemId,
            $this->faker()->numberBetween(1, 100000),
            Pricing::forSupplierPrice($this->faker()->randomFloat(2, 1000, 10000)),
            1,
            new Dimensions(
                $mass = $this->faker()->randomFloat(2, 0.05, 0.50),
                $this->faker()->numberBetween(1, 8)
            ),
            new Diamonds("brylantowy", $this->faker()->colorName, 1, $mass),
            new Other($this->faker()->realText(), null, null, "PL")
        );
    }

    private function newSingleVariant(?int $categoryId = null): Variant
    {
        $categoryId = $categoryId ?: $this->categoryId();

        return Variant::forCategory(
            $categoryId,
            "Diament-". $this->faker()->numberBetween(1000, 9999),
            $this->faker()->numberBetween(1, 100000),
            Pricing::forSupplierPrice($this->faker()->randomFloat(2, 1000, 10000)),
            1,
            new Dimensions(
                $mass = $this->faker()->randomFloat(2, 0.05, 0.50),
                $this->faker()->numberBetween(1, 8)
            ),
            new Diamonds("brylantowy", $this->faker()->colorName, 1, $mass),
            new Other($this->faker()->realText(), null, null, "PL")
        );
    }

    private function categoryId(): int
    {
        $categories = @explode(",", getenv("rodium.supplier.categories")) ?: [];
        $categories = array_values(
            array_unique(
                array_filter($categories, function($categoryId) { return (bool)trim($categoryId);})
            )
        );
        if (!$categories) {
            $this->markTestSkipped("rodium.supplier.categories must be set.");
        }

        return (int)$categories[$this->faker()->numberBetween(0, count($categories) - 1)];
    }

    private function diamondImage(?int $index = null): ImageSource
    {
        $index = $index ?: $this->faker()->numberBetween(1, 4);
        if ($index < 1 || $index > 4) {
            throw new \InvalidArgumentException(sprintf("Image inde must be between 1 and 4 but %d given.", $index));
        }

        return ImageSource::fromUrl(
          sprintf("https://gitlab.com/rodium-ec-pub/fixtures-photos/-/raw/master/diamonds/brylant_%d.jpg", $index)
        );
    }
}
