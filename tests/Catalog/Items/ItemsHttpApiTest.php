<?php

namespace Rodium\Sdk\Catalog\Items;

use Rodium\Sdk\AbstractIntegrationTest;
use Rodium\Sdk\Page\PageRequest;

class ItemsHttpApiTest extends AbstractIntegrationTest
{
    /** @var ItemsHttpApi */
    private $itemsApi;

    protected function setUp(): void
    {
        $this->itemsApi = $this->apiFactory()->itemsApi();
    }

    /**
     * @test
     */
    public function itGetsItemById()
    {
        $item = $this->itemsApi->itemOfId($this->itemId());

        $this->assertInstanceOf(Item::class, $item);
        $this->assertEquals($this->itemId(), $item->id());
    }

    /**
     * @test
     */
    public function itReturnsNullOnNonExistingItem()
    {
        $item = $this->itemsApi->itemOfId(-1000);

        $this->assertNull($item);
    }

    /**
     * @test
     */
    public function itGetsThePageOfItems()
    {
        $page = $this->itemsApi->items(PageRequest::defaultPageRequest());
        $elements = $page->elements();
        $this->assertNotEmpty($elements);
        $this->assertInstanceOf(Item::class, array_shift($elements));
    }

    /**
     * @test
     */
    public function itGetsThePageOfItemsOfGivenCategory()
    {
        $page = $this->itemsApi->items(PageRequest::defaultPageRequest(), $this->categoryId());
        $elements = $page->elements();

        $this->assertNotEmpty($elements);
        foreach ($page as $item) {
            $this->assertEquals($this->categoryId(), $item->categoryId());
        }
    }

    /**
     * @test
     */
    public function itGetsTheVariantsOfTheItem()
    {
        $variants = $this->itemsApi->variantsOfItem($this->itemId());
        $this->assertNotEmpty($variants);
        $this->assertInstanceOf(Variant::class, array_shift($variants));
    }

    private function categoryId(): int
    {
        $id = getenv("rodium.items.category_id");
        if ($id == null) {
            $this->markTestSkipped("\"rodium.items.category_id\" must be set.");
        }

        return (int)$id;
    }

    private function itemId(): int
    {
        $id = getenv("rodium.items.item_id");
        if ($id == null) {
            $this->markTestSkipped("\"rodium.items.item_id\" must be set.");
        }

        return (int)$id;
    }
}