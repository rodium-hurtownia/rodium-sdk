# Rodium SDK

Biblioteka dostarcza oficjalne narzędzie do integracji z API Hurtowni Rodium

## Spis treści
[Wymagania](#wymagania)

[Instalacja](#instalacja)

[Użytkowanie](#użytkowanie)

[API Kategorii](#api-kategorii--categoriesapi)

[API Produktów](#api-produktów--itemsapi)

[API Zamówień](#api-zamówień--ordersapi)

[API Dostawcy: Kategorie](#api-kategorii-dostawcy)

[API Dostawcy: Produktu](#api-produktów-dostawcy)

## Wymagania
 * PHP >= 7.2
 * Rozszerzenie `php-json`

## Instalacja

Zalacamy instalację używając narzędzia [Composer](https://getcomposer.org/download/)

```shell
composer require rodium/rodium-sdk
```

## Użytkowanie

Utwórz obiekt `ApiFactory`
Wymagany jest adres API (`baseUrl`) oraz klucz (`apiKey`). Skontaktuj się z Hurtownią Rodium w celu ich uzyskania.

```php
use Rodium\Sdk\Client\ApiFactory;
use Rodium\Sdk\Client\ApiKey;

$apiFactory = new ApiFactory("https://base-url", new ApiKey("twój-klucz-api"));
```

`ApiFactory` umożliwia utworzenie poszczególnych API:

```php
$categoriesApi = $apiFactory->categoriesApi(); // CategoriesApi
$itemsApi = $apiFactory->itemsApi(); // ItemsApi
$ordersApi = $apiFactory->ordersApi(); // OrdersApi

$supplierCategoriesApi = $apiFactory->supplierCategoriesApi(); // SupplierCategoriesApi
$supplierItemsApi = $apiFactory->supplierItemsApi(); // SupplierItemsApi  
```

## API Kategorii: CategoriesApi 

API Kategorii pozwala na pobianie inforacji o kategoriach katalogu Hurtowni 

### Pobieanie kategorii według ID

```php
$category = $categoriesApi->categoryOfId(123);
```

### Pobieanie płaskiej listy wszystkich kategorii

```php
$categories = $categoriesApi->all();
```

### Pobieanie pełnego drzewa kategorii

```php
$category = $categoriesApi->tree();
```

### Pobieanie drzewa wybranej kategorii

```php
$category = $categoriesApi->tree(123);
```

### Pobieanie dzieci wybranej kategorii

```php
$categories = $categoriesApi->children(123);
```

## API Produktów: ItemsApi 

API Produktów pozwala na pobianie inforacji o produktach oraz ich wariantach

### Pobieranie produktu według ID

Uwaga: Identyfikator produktu w API *NIE JEST* tożsamy z identyfikatorem produktu w Hurtowni Rodium

```php
$item = $itemsApi->itemOfId(123);
```

### Pobieranie listy produktów

```php
use Rodium\Sdk\Page\PageRequest;

$pageRequest = new PageRequest(1, 25); // numer strony, liczba produktów na stronę
// lub
$pageRequest = PageRequest::defaultPageRequest();
$page = $itemsApi->items($pageRequest);
```

### Pobieranie listy produktów (filtrowanie wegług kategorii)

```php
$pageRequest = PageRequest::defaultPageRequest();
$categoryId = 123;

$page = $itemsApi->items($pageRequest, $categoryId);
```

### Pobieranie wariantów produktu

Uwaga: Identyfikator wariantu w API *NIE JEST* tożsamy z identyfikatorem produktu w Hurtowni Rodium. Obiekt `Variant` posiada dedykowane pole `rodiumId`.

```php
$variants = $itemsApi->variantsOfItem(123);
```

## API Zamówień: OrdersApi

API Zamówień pozwala na pobianie inforacji o złożonych zamówieniach oraz składanie nowych

### Tworzenie API

Jeśli kontrahent posiada tylko jeden oddział:
```php
/** @var OrdersApi $ordersApi */
$ordersApi = $apiFactory->ordersApi();
```

Jeśli kontrahent posiada więcej niż jeden oddział, należy podać ID oddziały, dla którego API ma być utworzone
```php
/** @var OrdersApi $ordersApi */
$ordersApi = $apiFactory->ordersApi(123);
```

### Pobieranie listy zamówień

```php
use Rodium\Sdk\Page\PageRequest;

$orders = $ordersApi->orders();
// lub
$pageRequest = PageRequest::pageRequest(1, 30);
$orders = $ordersApi->orders($pageRequest);
```

### Pobieranie zamówienia według ID

Uwaga: Identyfikatorem zamówienia w API jest UUID (do odczytania poprzez `Order::id()`), a nie kolejny numer widoczny w intefejsie użytkownika. 

```php
$order = $ordersApi->orderOfId("id-zamówienia");
```

### Składanie zamówienia

UWAGA: Jeśli dane fakturowe nie są ustawione, zostną użyte dane z ostatniego zamówienia.

```php
use Rodium\Sdk\Order\Order;
use Rodium\Sdk\Order\InvoiceData;

// Dane fakturowe
$invoiceData = new InvoiceData(
    "Nazwa firmy",
    "1234567890", // NIP
    "ul. Mokra 2",
    "32-020",
    "Wieliczka"
); // opcjonalnie

$order = Order::newOrder($invoiceData);
$order = $order
    ->withVariant(123, 2)
    ->withVariant(234, 1)
    // ...
    ->withVariant(5431, 2)
    ->withNotes("Proszę o szybką realizację");

$placedOrder = $ordersApi->place($order);
```

### Obsługa błędów

W razie niepowodzenia, API wyrzuca wyjątki klasy `AbstractApiException`, dlatego każda operacja
powinna być wywołana w bloku `try/catch`.

Jeśli API zwróciło proprawną odpowiedź JSON, można się do niej dostać używając metody `AbstractApiException::error()`. 

```php
use Rodium\Sdk\Client\AbstractApiException;
use Rodium\Sdk\Error\ValidationError;

try {
    $placedOrder = $ordersApi->place($order);
} catch (AbstractApiException $e) {
    $request = $e->request(); // żądanie wysłane do API
    $response = $e->response(); // surowa odpowiedź z API

    $error = $e->error(); // przetworzona odpowiedź z błędem
    $errorMessage = $error->message(); // informacja o błędzie
    
    // jeśli błąd walidacji danych wejściowych
    if ($error instanceof ValidationError) {
        $violations = $error->violations(); // lista błędów walidacji
    }
}
```

## API Kategorii Dostawcy

API Kategorii Dostawcy pozwala na sprawdzenie, do których kategorii dostawca ma dostęp.

### Tworzenie API

Aby utworzyć API Produktów, użyj kodu poniżej

```php
use Rodium\Sdk\Catalog\Categories\SupplierCategoriesApi;

/** @var SupplierCategoriesApi $supplierApi */
$supplierApi = $apiFactory->supplierCategoriesApi();
```

### Sprawdzanie dostępnych kategorii przez drzewo

```php
use Rodium\Sdk\Catalog\Categories\SupplierCategoriesApi;

$rootCategory = $supplierApi->tree();
$rootCategory->isHasSupplierAccess(); // sprawdza, czy dostawca ma dostęp do tej kategorii

foreach ($rootCategory->children() as $child) {
    $child->isHasSupplierAccess();
    foreach ($child->children() as $child2) {
        $child2->isHasSupplierAccess();
        // ...
    }
}
```

### Sprawdzanie dostępnych kategorii przez płaską listę

```php
$categories = $supplierApi->all(); // zwraca płaską listę kategorii (liści), do których dostawca ma dostęp
```

## API Produktów Dostawcy

API Dostawcy pozwala na zarządzanie produktami i wariantami w przydzielonych kategoriach (dodawanie, edycja, usuwanie).

### Tworzenie API

Aby utworzyć API Produktów, użyj kodu poniżej 

```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;

/** @var SupplierItemsApi $supplierApi */
$supplierApi = $apiFactory->supplierItemsApi();
```

### Dodawanie nowych produktów i wariantów

Dodawanie nowego produktu
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Catalog\Items\Item;
/** @var SupplierItemsApi $supplierApi */

$item = Item::forCategory(
    123, // ID Kategorii
    "Produkt-123" // Symbol produktu nie może zawierać spacji
);

$createdItem = $supplierApi->createItem($item);
$createdItem->id();
```

Dodawanie wariantu do istniejącego produktu
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Catalog\Items\Variant;
use Rodium\Sdk\Catalog\Items\Pricing;
use Rodium\Sdk\Catalog\Items\Dimensions;
use Rodium\Sdk\Catalog\Items\Diamonds;
use Rodium\Sdk\Catalog\Items\Other;

/** @var SupplierItemsApi $supplierApi */

$variant = Variant::forItem(
    123, // ID Produktu
    "321", // ID Produktu w systemie dostawcy
    Pricing::forSupplierPrice(1234.23), // cena netto dostawcy
    1, // stan magazynowy (ilościowy)
    new Dimensions(
        2.43,
        10
    ), // wymiary (masa, średnica, ...)
    new Diamonds(
        "brylantowy", // szlif
        "biały", // barwa
        1, // liczba brylantów
        0.32 // masa brylantów
    ), // informacje o brynalcie
    new Other(
        "Pięny pierścionek", // dodatkowy opis słowny
        null, // kolor złota
        null, // opis innych kamieni szlachetnych
        "PL" // kraj producenta
    ), // inne informacje
    null // EAN
);

$createdVariant = $supplierApi->createVariant($variant);
$createdItem->id();
```

Dodawanie nowego produktu tylko z jednym wariantem 
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Catalog\Items\Variant;
use Rodium\Sdk\Catalog\Items\Pricing;
use Rodium\Sdk\Catalog\Items\Dimensions;
use Rodium\Sdk\Catalog\Items\Diamonds;
use Rodium\Sdk\Catalog\Items\Other;

/** @var SupplierItemsApi $supplierApi */

$variant = Variant::forCategory(
    123, // ID Kategorii
    "Produkt-321",
    "321", // ID Produktu w systemie dostawcy
    Pricing::forSupplierPrice(1234.23), // cena netto dostawcy
    1, // stan magazynowy (ilościowy)
    new Dimensions(
        2.43,
        10
    ), // wymiary (masa, średnica, ...)
    new Diamonds(
        "brylantowy", // szlif
        "biały", // barwa
        1, // liczba brylantów
        0.32 // masa brylantów
    ), // informacje o brynalcie
    new Other(
        "Pięny pierścionek", // dodatkowy opis słowny
        null, // kolor złota
        null, // opis innych kamieni szlachetnych
        "PL" // kraj producenta
    ), // inne informacje,
    null // EAN
);

$createdVariant = $supplierApi->createVariant($variant);
$createdItem->id();
```

### Zarządzanie zdjęciami wariantów

Dodawanie zdjęć
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Catalog\Items\ImageSource;

/** @var SupplierItemsApi $supplierApi */
$variant = $supplierApi->addVariantPhoto(
    321, // ID wariantu
    ImageSource::fromUrl("https://moj-sklep-jubilerski.pl/prodkuty/321-xxl.jpg")
);

$variant->images(); // list of images
```

Podmiana zdjęcia
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Catalog\Items\ImageSource;

/** @var SupplierItemsApi $supplierApi */
$variant = $supplierApi->changeVariantPhoto(
    321, // ID wariantu
    ImageSource::fromUrl("https://moj-sklep-jubilerski.pl/prodkuty/321-xxl.jpg"),
    0 // numer zdjęcia do podmiany (indeksowane od zera),  
);

$variant->images(); // list of images
```

Usuwanie zdjęcia
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;

/** @var SupplierItemsApi $supplierApi */
$variant = $supplierApi->removeVariantPhoto(
    321, // ID wariantu
    0 // numer zdjęcia do usunięcia (indeksowane od zera)
);

$variant->images(); // list of images
```

### Edycja wariantu

Poniższy kod nadpisuje WSZYSTKIE parametry danego wariantu.
Nie ma możliwości edycji poszczególnych parametrów oddzielnie.

```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;

/** @var SupplierItemsApi $supplierApi */
$upatedVariant = $supplierApi->updateVariant(
    321, // ID wariantu
    $variant // zobacz Variant::forItem
);
```

### Zmiana stanu magazynowego wariantu

Aby zmienić stan magazynowy wariantu, użyj kodu poniżej

```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;

/** @var SupplierItemsApi $supplierApi */
$upatedVariant = $supplierApi->updateVariantStock(
    321, // ID wariantu
    5 // liczba dostępnych sztuk
);
```

### Usuwanie produktów i wariantów

Aby usunąć istniejący wariant użyj kodu poniżej
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;

/** @var SupplierItemsApi $supplierApi */
$supplierApi->removeVariant(321);
```

Aby usunąć istniejący produkt (wraz z wariantami) użyj kodu poniżej
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;

/** @var SupplierItemsApi $supplierApi */
$supplierApi->removeItem(123);
```

### Listowanie produktów i wariantów

Pobieranie listy produktów dostawcy
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Page\PageRequest;

/** @var SupplierItemsApi $supplierApi */
$page = $supplierApi->items(
    PageRequest::pageRequest(1, 25)
);

/** @var \Rodium\Sdk\Catalog\Items\Item $item */
foreach ($page->elements() as $item) {
    $item->id();
}
```

Pobieranie wariantów danego produktu
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Page\PageRequest;

/** @var SupplierItemsApi $supplierApi */
$variants = $supplierApi->variantsOfItem(
    123 // ID Produktu
);

foreach ($variants as $variant) {
    $variant->id();
}
```

Pobieranie wybranego wariantu
```php
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Page\PageRequest;

/** @var SupplierItemsApi $supplierApi */
$variant = $supplierApi->variantOfId(
    123 // ID Wariantu
);

$variant->id();
```

## Zakończenie
W razie problemów / pytań, prosimy o kontakt z pracownikami Hurtowni Rodium [rodium@rodium.pl](mailto:rodium@rodium.pl)
