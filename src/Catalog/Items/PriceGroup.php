<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class PriceGroup
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("symbol")
     * @JMS\Groups("default")
     */
    private $symbol;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("value")
     * @JMS\Groups("default")
     */
    private $value;

    public function __construct(string $symbol, float $value)
    {
        $this->symbol = $symbol;
        $this->value = $value;
    }

    public function symbol(): string
    {
        return $this->symbol;
    }

    public function value(): float
    {
        return $this->value;
    }
}