<?php

namespace Rodium\Sdk\Catalog\Items;

final class StockLevel
{
    private const LACK = "lack";
    private const RUNNING_LOW = "running_low";
    private const AVAILABLE = "available";

    /** @var string */
    private $level;

    private function __construct(string $level)
    {
        $this->level = $level;
    }

    public static function lack(): self
    {
        return new self(self::LACK);
    }

    public static function runningLow(): self
    {
        return new self(self::RUNNING_LOW);
    }

    public static function available(): self
    {
        return new self(self::AVAILABLE);
    }

    public function __toString(): string
    {
        return $this->level;
    }

    public static function fromString(string $level): self
    {
        switch (trim(strtolower($level))) {
            case self::LACK:
                return self::lack();
            case self::RUNNING_LOW:
                return self::runningLow();
            case self::AVAILABLE:
                return self::available();
            default:
                throw new \OutOfBoundsException(sprintf("Unknown StockLevel \"%s\"", $level));
        }
    }
}