<?php

namespace Rodium\Sdk\Catalog\Items;

use Rodium\Sdk\Page\Page;
use Rodium\Sdk\Page\PageRequest;

interface SupplierItemsApi
{
    /**
     * Creates a new item
     *
     * @param Item $item the item to be created
     * @return Item the created item
     */
    public function createItem(Item $item): Item;

    /**
     * Removes the item of given ID
     *
     * @param int $itemId the ID of th item to be removed
     * @return void
     */
    public function removeItem(int $itemId): void;

    /**
     * Gets the paginated list of items managed by the supplier
     *
     * @param PageRequest|null $pageRequest the requested page
     * @return Page the page containing items managed by the supplier
     */
    public function items(PageRequest $pageRequest = null): Page;

    /**
     * Gets the un-paginated list of variants of given item
     *
     * @param int $itemId the ID of the item variants should be return for
     * @return Variant[] the array containing variants
     */
    public function variantsOfItem(int $itemId): array;

    /**
     * Gets the variant of given ID
     *
     * @param int $variantId the ID of the variant
     * @return ?Variant the requested variant or null if nor found
     */
    public function variantOfId(int $variantId): ?Variant;

    /**
     * Creates a new variant
     *
     * @param Variant $variant the variant to be created
     * @return Variant the created variant
     */
    public function createVariant(Variant $variant): Variant;

    /**
     * Updated the variant of given ID
     *
     * @param int $variantId the ID of the variant to be updated
     * @param Variant $variant the variant request
     * @return Variant the updated variant
     */
    public function updateVariant(int $variantId, Variant $variant): Variant;

    /**
     * Removes the variant of given ID
     *
     * @param int $variantId the ID of the variant to be removed
     */
    public function removeVariant(int $variantId): void;

    /**
     * Adds a new photo to the variant
     *
     * @param int $variantId the ID of the variant the photo should be added to
     * @param ImageSource $imageSource the source of the image
     * @return Variant the updated variant
     */
    public function addVariantPhoto(int $variantId, ImageSource $imageSource): Variant;

    /**
     * Changes the photo of given index on the variant of given ID
     *
     * @param int $variantId the ID of the variant the photo should be changed on
     * @param ImageSource $imageSource the source of the image
     * @param int $index the index of the photo to be changed (0-based)
     * @return Variant the updated variant
     */
    public function changeVariantPhoto(int $variantId, ImageSource $imageSource, int $index): Variant;

    /**
     * Changes the photo of given index from the variant of given ID
     *
     * @param int $variantId the ID of the variant the photo should be removed from
     * @param int $index the index of the photo to be removed (0-based)
     * @return Variant the updated variant
     */
    public function removeVariantPhoto(int $variantId, int $index): Variant;

    /**
     * Updated the stock of variant of given ID
     *
     * @param int $variantId the ID of the variant the stock is to be updated
     * @param float $quantity the new stock quantity
     * @return Variant the updated variant
     */
    public function updateVariantStock(int $variantId, float $quantity): Variant;
}
