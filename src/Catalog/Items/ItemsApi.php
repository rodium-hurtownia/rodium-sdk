<?php

namespace Rodium\Sdk\Catalog\Items;

use Rodium\Sdk\Client\ResourceNotFoundException;
use Rodium\Sdk\Page\Page;
use Rodium\Sdk\Page\PageRequest;

/**
 * Provides the wrapper for Items API operations
 */
interface ItemsApi
{
    /**
     * Gets the Item of given ID
     *
     * @param int $id the ID of the Item to be fetched
     * @return ?Item the Item of given ID, null if the Item does not exist
     */
    public function itemOfId(int $id): ?Item;

    /**
     * Gets the Page of Items
     *
     * @param ?PageRequest $pageRequest the requested page
     * @param ?int $categoryId the ID of the category Items should be fetched for
     * @throws ResourceNotFoundException if page does not exist
     */
    public function items(?PageRequest $pageRequest = null, ?int $categoryId = null): ?Page;

    /**
     * Gets the list of Variants of given Item ID
     *
     * @param int $id the ID of the Item variants should fetch of
     * @return Variant[] the list of Variants
     * @throws ResourceNotFoundException if Item of given ID does not exist
     */
    public function variantsOfItem(int $id): array;
}