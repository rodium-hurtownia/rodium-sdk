<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

/**
 * Represents the image source for the variant
 */
final class ImageSource
{
    /**
     * @var string
     * @JMS\SerializedName("url")
     * @JMS\Type("string")
     * @JMS\Groups({"create", "update"})
     */
    private $url;

    private function __construct(string $url)
    {
        $this->url = $url;
    }

    public static function fromUrl(string $url): self
    {
        return new self($url);
    }

    public function url(): string
    {
        return $this->url;
    }

    public function __toString(): string
    {
        return $this->url;
    }
}
