<?php

namespace Rodium\Sdk\Catalog\Items;

use Rodium\Sdk\Client\AbstractApi;
use Rodium\Sdk\Client\ResourceNotFoundException;
use Rodium\Sdk\Page\Page;
use Rodium\Sdk\Page\PageRequest;

/**
 * Provides the HTTP implementation of the ItemsApi
 */
final class ItemsHttpApi extends AbstractApi implements ItemsApi
{
    /**
     * @inheritDoc
     */
    public function itemOfId(int $id): ?Item
    {
        try {
            return $this->get(sprintf("/items/%d", $id), Item::class);
        } catch (ResourceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function items(?PageRequest $pageRequest = null, ?int $categoryId = null): ?Page
    {
        $pageRequest = $pageRequest ?? PageRequest::defaultPageRequest();
        $path = sprintf("/items?%s", $pageRequest->toQueryString());
        $path = $categoryId ? sprintf("%s&category_id=%d", $path, $categoryId) : $path;

        try {
            return $this->get($path, sprintf("%s<%s>", Page::class, Item::class));
        } catch (ResourceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function variantsOfItem(int $id): array
    {
        return $this->get(sprintf("/items/%s/variants", $id), sprintf("array<%s>", Variant::class));
    }
}
