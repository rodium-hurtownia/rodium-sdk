<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class Other
{
    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("notes")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $notes;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("gold_hue")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $goldHue;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("gem_stones")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $gemStones;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("manufacturer_country")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $manufacturerCountry;

    public function __construct(
        ?string $notes = null,
        ?string $goldHue = null,
        ?string $gemStones = null,
        ?string $manufacturerCountry = null
    ) {
        $this->notes = $notes ?? null;
        $this->goldHue = $goldHue ?? null;
        $this->gemStones = $gemStones ?? null;
        $this->manufacturerCountry = $manufacturerCountry ?? null;
    }

    public function notes(): ?string
    {
        return $this->notes;
    }

    public function goldHue(): ?string
    {
        return $this->goldHue;
    }

    public function gemStones(): ?string
    {
        return $this->gemStones;
    }

    public function manufacturerCountry(): ?string
    {
        return $this->manufacturerCountry;
    }
}