<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class Diamonds
{
    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("cut")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $cut;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("hue")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $hue;

    /**
     * @var int|null
     * @JMS\Type("int")
     * @JMS\SerializedName("quantity")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $quantity;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("mass")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $mass;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\SerializedName("notes")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $notes;

    public function __construct(
        ?string $cut = null,
        ?string $hue = null,
        ?int $quantity = null,
        ?float $mass = null,
        ?string $notes = null
    ) {
        $this->cut = $cut;
        $this->hue = $hue;
        $this->quantity = $quantity;
        $this->mass = $mass;
        $this->notes = $notes;
    }

    public function cut(): ?string
    {
        return $this->cut;
    }

    public function hue(): ?string
    {
        return $this->hue;
    }

    public function quantity(): ?int
    {
        return $this->quantity;
    }

    public function mass(): ?float
    {
        return $this->mass;
    }

    public function notes(): ?string
    {
        return $this->notes;
    }
}