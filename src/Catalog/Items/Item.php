<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class Item
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"default"})
     */
    private $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("symbol")
     * @JMS\Groups({"default", "create"})
     */
    private $symbol;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("category_id")
     * @JMS\Groups({"default", "create"})
     */
    private $categoryId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("stock_level")
     */
    private $stockLevel;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("image_url")
     */
    private $imageUrl;

    private function __construct(?int $id, string $symbol, int $categoryId, ?StockLevel $stockLevel = null, ?string $imageUrl = null)
    {
        $this->id = $id;
        $this->symbol = $symbol;
        $this->categoryId = $categoryId;
        $this->stockLevel = (string)$stockLevel;
        $this->imageUrl = $imageUrl;
    }

    public static function forCategory(int $categoryId, string $symbol): Item
    {
        return new self(
            null,
            $symbol,
            $categoryId
        );
    }

    public function id(): int
    {
        return $this->id;
    }

    public function symbol(): string
    {
        return $this->symbol;
    }

    public function categoryId(): int
    {
        return $this->categoryId;
    }

    public function stockLevel(): StockLevel
    {
        return StockLevel::fromString($this->stockLevel);
    }

    public function imageUrl(): ?string
    {
        return $this->imageUrl;
    }
}