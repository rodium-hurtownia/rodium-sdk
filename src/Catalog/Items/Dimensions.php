<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class Dimensions
{
    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("mass")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $mass;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("diameter")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $diameter;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("length")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $length;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("width")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $width;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("height")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $height;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("element_width")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $elementWidth;

    /**
     * @var float|null
     * @JMS\Type("float")
     * @JMS\SerializedName("element_height")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $elementHeight;

    public function __construct(
        ?float $mass = null,
        ?float $diameter = null,
        ?float $length = null,
        ?float $width = null,
        ?float $height = null,
        ?float $elementWidth = null,
        ?float $elementHeight = null
    ) {
        $this->mass = $mass;
        $this->diameter = $diameter;
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->elementWidth = $elementWidth;
        $this->elementHeight = $elementHeight;
    }

    public function mass(): ?float
    {
        return $this->mass;
    }

    public function diameter(): ?float
    {
        return $this->diameter;
    }

    public function length(): ?float
    {
        return $this->length;
    }

    public function width(): ?float
    {
        return $this->width;
    }

    public function height(): ?float
    {
        return $this->height;
    }

    public function elementWidth(): ?float
    {
        return $this->elementWidth;
    }

    public function elementHeight(): ?float
    {
        return $this->elementHeight;
    }
}