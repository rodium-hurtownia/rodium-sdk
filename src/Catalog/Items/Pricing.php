<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class Pricing
{
    /**
     * @var PriceGroup
     * @JMS\Type("Rodium\Sdk\Catalog\Items\PriceGroup")
     * @JMS\SerializedName("price_group")
     * @JMS\Groups({"default"})
     */
    private $priceGroup;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("supplier_price")
     * @JMS\Groups({"create", "update", "supplier"})
     */
    private $supplierPrice;

    private function __construct(?PriceGroup $priceGroup = null, ?float $supplierPrice = null)
    {
        $this->priceGroup = $priceGroup;
        $this->supplierPrice = $supplierPrice;
    }

    public static function forSupplierPrice(float $supplierPrice): Pricing
    {
        return new self(null, $supplierPrice);
    }

    public function priceGroup(): ?PriceGroup
    {
        return $this->priceGroup;
    }

    public function supplierPrice(): ?float
    {
        return $this->supplierPrice;
    }
}