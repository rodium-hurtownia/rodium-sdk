<?php

namespace Rodium\Sdk\Catalog\Items;

use Rodium\Sdk\Client\AbstractApi;
use Rodium\Sdk\Client\RequestData;
use Rodium\Sdk\Page\Page;
use Rodium\Sdk\Page\PageRequest;

final class SupplierItemsHttpApi extends AbstractApi implements SupplierItemsApi
{
    /**
     * @inheritDoc
     */
    public function createItem(Item $item): Item
    {
        return $this->post("/supplier/items", RequestData::withGroups($item, ["create"]), Item::class);
    }

    /**
     * @inheritDoc
     */
    public function removeItem(int $itemId): void
    {
        $this->delete(sprintf("/supplier/items/%d", $itemId));
    }

    /**
     * @inheritDoc
     */
    public function items(PageRequest $pageRequest = null): Page
    {
        $pageRequest = $pageRequest ?? PageRequest::defaultPageRequest();
        $path = sprintf("/supplier/items?%s", $pageRequest->toQueryString());

        return $this->get($path, sprintf("%s<%s>", Page::class, Item::class));
    }

    /**
     * @inheritDoc
     */
    public function variantsOfItem(int $itemId): array
    {
        $path = sprintf("/supplier/items/%d/variants", $itemId);

        return $this->get($path, sprintf("array<%s>", Variant::class));
    }

    /**
     * @inheritDoc
     */
    public function variantOfId(int $variantId): ?Variant
    {
        return $this->get(sprintf("/supplier/items/variants/%d", $variantId), Variant::class);
    }

    /**
     * @inheritDoc
     */
    public function createVariant(Variant $variant): Variant
    {
        return $this->post(
            "/supplier/items/variants",
            RequestData::withGroups($variant, ["create"]),
            Variant::class
        );
    }

    /**
     * @inheritDoc
     */
    public function updateVariant(int $variantId, Variant $variant): Variant
    {
        return $this->put("/supplier/items/variants", $variant, Variant::class);
    }

    /**
     * @inheritDoc
     */
    public function removeVariant(int $variantId): void
    {
        $this->delete(sprintf("/supplier/items/variants/%d", $variantId));
    }

    /**
     * @inheritDoc
     */
    public function addVariantPhoto(int $variantId, ImageSource $imageSource): Variant
    {
        return $this->post(
            sprintf("/supplier/items/variants/%d/photos", $variantId),
            $imageSource,
            Variant::class
        );
    }

    /**
     * @inheritDoc
     */
    public function changeVariantPhoto(int $variantId, ImageSource $imageSource, int $index): Variant
    {
        return $this->put(
            sprintf("/supplier/items/variants/%d/photos/%d", $variantId, $index),
            $imageSource,
            Variant::class
        );
    }

    /**
     * @inheritDoc
     */
    public function removeVariantPhoto(int $variantId, int $index): Variant
    {
        return $this->delete(
            sprintf("/supplier/items/variants/%d/photos/%d", $variantId, $index),
            Variant::class
        );
    }

    /**
     * @inheritDoc
     */
    public function updateVariantStock(int $variantId, float $quantity): Variant
    {
        return $this->put(
            sprintf("/supplier/items/variants/%d/stock", $variantId),
            ["stock" => $quantity],
            Variant::class
        );
    }
}
