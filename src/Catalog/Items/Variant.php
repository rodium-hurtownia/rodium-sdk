<?php

namespace Rodium\Sdk\Catalog\Items;

use JMS\Serializer\Annotation as JMS;

final class Variant
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"default"})
     */
    private $id;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("category_id")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $categoryId;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("item_id")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $itemId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("symbol")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $symbol;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("supplier_id")
     * @JMS\Groups({"create", "update", "supplier"})
     */
    private $supplierId;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("stock_quantity")
     * @JMS\Groups({"create", "update", "supplier"})
     */
    private $stockQuantity;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("stock_level")
     * @JMS\Groups({"default"})
     */
    private $stockLevel;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("ean")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $ean;

    /**
     * @var Pricing
     * @JMS\Type("Rodium\Sdk\Catalog\Items\Pricing")
     * @JMS\SerializedName("pricing")
     * @JMS\Groups({"create", "update", "supplier"})
     */
    private $pricing;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("price")
     * @JMS\Groups({"default"})
     */
    private $price;

    /**
     * @var Dimensions
     * @JMS\Type("Rodium\Sdk\Catalog\Items\Dimensions")
     * @JMS\SerializedName("dimensions")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $dimensions;

    /**
     * @var Diamonds
     * @JMS\Type("Rodium\Sdk\Catalog\Items\Diamonds")
     * @JMS\SerializedName("diamonds")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $diamonds;

    /**
     * @var Other
     * @JMS\Type("Rodium\Sdk\Catalog\Items\Other")
     * @JMS\SerializedName("other")
     * @JMS\Groups({"default", "create", "update"})
     */
    private $other;

    /**
     * @var string[]
     * @JMS\Type("array<string>")
     * @JMS\SerializedName("images")
     * @JMS\Groups({"default"})
     */
    private $images;

    private function __construct(
        ?int $itemId = null,
        ?int $categoryId = null,
        ?string $symbol = null,
        ?int $supplierId = null,
        ?Pricing $pricing = null,
        ?float $stockQuantity = null,
        ?Dimensions $dimensions = null,
        ?Diamonds $diamonds = null,
        ?Other $other = null,
        ?string $ean = null,
        array $images = []
    ) {
        $this->itemId = $itemId;
        $this->categoryId = $categoryId;
        $this->symbol = $symbol;
        $this->supplierId = $supplierId;
        $this->ean = $ean;
        $this->stockQuantity = $stockQuantity;
        $this->pricing = $pricing;
        $this->dimensions = $dimensions ?? new Dimensions();
        $this->diamonds = $diamonds ?? new Diamonds();
        $this->other = $other ?? new Other();
        $this->images = $images;
    }

    public static function forCategory(
        int $categoryId,
        string $symbol,
        string $supplierId,
        Pricing $pricing,
        float $stockQuantity = 0,
        ?Dimensions $dimensions = null,
        ?Diamonds $diamonds = null,
        ?Other $other = null,
        ?string $ean = null
    ): self {
        return new self(
            null,
            $categoryId,
            $symbol,
            $supplierId,
            $pricing,
            $stockQuantity,
            $dimensions,
            $diamonds,
            $other,
            $ean
        );
    }

    public static function forItem(
        int $itemId,
        string $supplierId,
        Pricing $pricing,
        float $stockQuantity = 0,
        ?Dimensions $dimensions = null,
        ?Diamonds $diamonds = null,
        ?Other $other = null,
        ?string $ean = null
    ): self {
        return new self(
            $itemId,
            null,
            null,
            $supplierId,
            $pricing,
            $stockQuantity,
            $dimensions,
            $diamonds,
            $other,
            $ean
        );
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function itemId(): ?int
    {
        return $this->itemId;
    }

    public function symbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @deprecated use supplierId() instead
     */
    public function rodiumId(): ?string
    {
        return $this->supplierId;
    }

    public function supplierId(): ?string
    {
        return $this->supplierId;
    }

    public function stockLevel(): ?StockLevel
    {
        return $this->stockLevel ? StockLevel::fromString($this->stockLevel) : null;
    }

    public function stockQuantity(): ?float
    {
        return $this->stockQuantity;
    }

    public function ean(): ?string
    {
        return $this->ean;
    }

    /**
     * @deprecated Use pricing() instead
     */
    public function priceGroup(): ?PriceGroup
    {
        return $this->pricing;
    }

    public function dimensions(): Dimensions
    {
        return $this->dimensions;
    }

    public function diamonds(): Diamonds
    {
        return $this->diamonds;
    }

    public function other(): Other
    {
        return $this->other;
    }

    public function images(): array
    {
        return $this->images;
    }
}