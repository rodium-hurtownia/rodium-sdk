<?php

namespace Rodium\Sdk\Catalog\Categories;

/**
 * Provides the wrapper for Categories API operations allowed for supplier
 */
interface SupplierCategoriesApi
{
    /**
     * Gets the tree of categories the supplier has access to
     *
     * @return Category the root category with children
     */
    public function tree(): Category;

    /**
     * Gets the flat list of leaf categories the supplier has access to
     *
     * @return Category[]
     */
    public function all(): array;
}
