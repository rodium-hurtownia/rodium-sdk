<?php

namespace Rodium\Sdk\Catalog\Categories;

use Rodium\Sdk\Client\AbstractApi;

final class SupplierCategoriesHttpApi extends AbstractApi implements SupplierCategoriesApi
{
    /**
     * @inheritDoc
     */
    public function tree(): Category
    {
        return $this->get("/supplier/categories/tree", Category::class);
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return $this->get("/supplier/categories", sprintf("array<%s>", Category::class));
    }
}
