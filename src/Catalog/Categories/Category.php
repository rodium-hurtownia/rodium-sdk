<?php

namespace Rodium\Sdk\Catalog\Categories;

use JMS\Serializer\Annotation as JMS;

/**
 * Represents the Category the Items are assigned to
 */
final class Category
{
    /**
     * The Category identifier
     *
     * @var int
     * @JMS\Type("int")
     */
    private $id;

    /**
     * The Category name
     *
     * @var string
     * @JMS\Type("string")
     */
    private $name;

    /**
     * The Category path in the tree
     *
     * @var string
     * @JMS\Type("string")
     */
    private $path;

    /**
     * The URL to the Category image
     *
     * @var string
     * @JMS\Type("string")
     */
    private $imageUrl;

    /**
     * The flag indicating if supplier has access to this category
     *
     * @var bool
     * @JMS\Type("bool")
     */
    private $hasSupplierAccess;

    /**
     * List of children categories
     *
     * @var Category[]
     * @JMS\Type("array<Rodium\Sdk\Catalog\Categories\Category>")
     */
    private $children;

    public function __construct(
        int $id,
        string $name,
        string $path,
        ?string $imageUrl = null,
        bool $hasSupplierAccess = false,
        array $children = []
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->path = $path;
        $this->imageUrl = $imageUrl;
        $this->hasSupplierAccess = $hasSupplierAccess;
        $this->children = $children;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function path(): string
    {
        return $this->path;
    }

    public function imageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function children(): array
    {
        return $this->children;
    }

    public function isHasSupplierAccess(): bool
    {
        return (bool)$this->hasSupplierAccess;
    }
}
