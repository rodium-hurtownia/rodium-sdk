<?php

namespace Rodium\Sdk\Catalog\Categories;

use Rodium\Sdk\Client\ResourceNotFoundException;

/**
 * Provides the wrapper for Categories API operations
 */
interface CategoriesApi
{
    /**
     * Gets the Category by its ID
     * Returns null if not found
     *
     * @param int $id the ID of the Category to be fetched
     * @return Category|null the Category (null if does not exist)
     */
    public function categoryOfId(int $id): ?Category;

    /**
     * Gets the all Categories
     *
     * @return Category[] the list of all the Categories
     */
    public function all(): array;

    /**
     * Gets the tree of the Categories or subtree of the given Category
     *
     * @param ?int $id the ID of the Category the subtree to be fetched of
     * @return Category the Category contains children of all the levels
     * @throws ResourceNotFoundException if Category of given ID does not exist
     */
    public function tree(?int $id = null): Category;

    /**
     * Gets the direct children of given Category
     *
     * @param ?int $id the ID of the Category the subtree to be fetched of
     * @throws ResourceNotFoundException if Category of given ID does not exist
     */
    public function children(int $id): array;
}