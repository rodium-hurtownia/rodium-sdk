<?php

namespace Rodium\Sdk\Catalog\Categories;

use Rodium\Sdk\Client\AbstractApi;
use Rodium\Sdk\Client\ResourceNotFoundException;

/**
 * Provides the HTTP implementation of the CategoriesApi
 */
final class CategoriesHttpApi extends AbstractApi implements CategoriesApi
{
    /**
     * @inheritDoc
     */
    public function categoryOfId(int $id): ?Category
    {
        try {
            return $this->get(sprintf("/categories/%d", $id), Category::class);
        } catch (ResourceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return $this->get("/categories", sprintf("array<%s>", Category::class));
    }

    /**
     * @inheritDoc
     */
    public function tree(int $id = null): Category
    {
        $path = $id ? sprintf("/categories/%d/tree", $id) : "/categories/tree";

        return $this->get($path,Category::class);
    }

    /**
     * @inheritDoc
     */
    public function children(int $id): array
    {
        return (array)$this->get(sprintf("/categories/%d/children", $id), sprintf("array<%s>", Category::class));
    }
}