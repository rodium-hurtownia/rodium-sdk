<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\Annotation as JMS;
use OpenApi\Annotations as OA;

final class OrderMetaData
{
    /**
     * The creation time
     *
     * @var \DateTime
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("created_at")
     */
    private $createdAt;

    /**
     * The status
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("status")
     */
    private $status;

    /**
     * The time the order was placed at
     *
     * @var \DateTime
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("placed_at")
     */
    private $placedAt;

    /**
     * The time the order was archived at
     *
     * @var \DateTime
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("archived_at")
     */
    private $archivedAt;

    /**
     * The time the order was closed at
     *
     * @var \DateTime
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("closed_at")
     */
    private $closedAt;

    public function __construct(
        ?\DateTime $createdAt,
        ?string $status,
        ?\DateTime $placedAt,
        ?\DateTime $archivedAt,
        ?\DateTime $closedAt
    ) {
        $this->createdAt = $createdAt;
        $this->status = $status;
        $this->placedAt = $placedAt;
        $this->archivedAt = $archivedAt;
        $this->closedAt = $closedAt;
    }

    public function createdAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function status(): ?string
    {
        return $this->status;
    }

    public function placedAt(): ?\DateTime
    {
        return $this->placedAt;
    }

    public function archivedAt(): ?\DateTime
    {
        return $this->archivedAt;
    }

    public function closedAt(): ?\DateTime
    {
        return $this->closedAt;
    }
}
