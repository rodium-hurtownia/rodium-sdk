<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\Annotation as JMS;

final class Item
{
    /**
     * The item ID
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     */
    private $id;

    /**
     * Describes the origin (source) item for this order item
     *
     * @var ItemOrigin
     * @JMS\Type("Rodium\Sdk\Order\ItemOrigin")
     * @JMS\SerializedName("origin")
     */
    private $origin;

    /**
     * The item quantity
     *
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("quantity")
     */
    private $quantity;

    /**
     * Additional note
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("note")
     */
    private $note = null;

    /**
     * The warning on this item
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("warning")
     */
    private $warning;

    public function __construct(ItemOrigin $origin, int $quantity, ?string $note = null)
    {
        $this->origin = $origin;
        $this->quantity = $quantity;
        $this->note = $note;
    }

    public static function variantItem(int $variantId, int $quantity, ?string $note = null): Item
    {
        return new self(ItemOrigin::variant($variantId), $quantity, $note);
    }

    public function id(): ?string
    {
        return $this->id;
    }

    public function origin(): ?ItemOrigin
    {
        return $this->origin;
    }

    public function quantity(): ?int
    {
        return $this->quantity;
    }

    public function note(): ?string
    {
        return $this->note;
    }

    public function warning(): ?string
    {
        return $this->warning;
    }
}
