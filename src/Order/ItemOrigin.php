<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\Annotation as JMS;

final class ItemOrigin
{
    /**
     * The name (type) of the origin item
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     */
    private $name = null;


    /**
     * The ID of the origin item
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     */
    private $id;

    /**
     * Attributes describing the origin item
     *
     * @var array
     * @JMS\Type("array")
     * @JMS\SerializedName("attributes")
     */
    private $attributes = [];

    public function __construct(string $name, string $id, array $attributes = [])
    {
        $this->name = $name;
        $this->id = $id;
        $this->attributes = $attributes;
    }

    public static function variant(int $variantId): self
    {
        return new self("variant", $variantId);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function attributes(): array
    {
        return $this->attributes;
    }

    public function __toString(): string
    {
        return sprintf("%s:%s", $this->name(), $this->id());
    }
}
