<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\Annotation as JMS;

final class Order
{
    /**
     * The ID of the order
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     */
    private $id;

    /**
     * The numeric ID of the order
     *
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("sequence_id")
     */
    private $sequenceId;

    /**
     * The invoice data
     *
     * @var InvoiceData|null
     * @JMS\Type("Rodium\Sdk\Order\InvoiceData")
     * @JMS\SerializedName("invoice_data")
     */
    private $invoiceData;

    /**
     * The items in the order
     *
     * @var array
     * @JMS\Type("array<Rodium\Sdk\Order\Item>")
     * @JMS\SerializedName("items")
     */
    private $items;

    /**
     * The numeric ID of the order
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("notes")
     */
    private $notes;

    /**
     * Net / gross total values
     *
     * @var OrderTotals
     * @JMS\Type("Rodium\Sdk\Order\OrderTotals")
     * @JMS\SerializedName("totals")
     */
    private $totals;

    /**
     * Order's meta data (status / event's dates)
     *
     * @var OrderMetaData
     * @JMS\Type("Rodium\Sdk\Order\OrderMetaData")
     * @JMS\SerializedName("meta_data")
     */
    private $metaData;

    private function __construct(
        ?InvoiceData $invoiceData,
        array $items = [],
        ?string $notes = null
    ) {
        $this->invoiceData = $invoiceData;
        $this->items = $items;
        $this->notes = $notes;
    }

    /**
     * Creates a new instance of the Order for with invoice data
     *
     * @param ?InvoiceData $invoiceData
     * @return Order
     */
    public static function newOrder(?InvoiceData $invoiceData = null): self
    {
        return new self($invoiceData);
    }

    public function withItem(Item $item): Order
    {
        $items = $this->items;
        $items[] = $item;

        return new self($this->invoiceData, $items, $this->notes);
    }

    public function withVariant(int $variantId, int $quantity, ?string $note = null): Order
    {
        return $this->withItem(Item::variantItem($variantId, $quantity, $note));
    }

    public function withInvoiceData(InvoiceData $invoiceData): Order
    {
        return new self($invoiceData, $this->items, $this->notes);
    }

    public function withoutInvoiceData(): Order
    {
        return new self(null, $this->items, $this->notes);
    }

    public function withNotes(string $notes): Order
    {
        return new self($this->invoiceData, $this->items, $notes);
    }

    public function id(): ?string
    {
        return $this->id;
    }

    public function sequenceId(): ?int
    {
        return $this->sequenceId;
    }

    public function invoiceData(): ?InvoiceData
    {
        return $this->invoiceData;
    }

    public function items(): array
    {
        return $this->items;
    }

    public function notes(): ?string
    {
        return $this->notes;
    }

    public function totals(): ?OrderTotals
    {
        return $this->totals;
    }

    public function metaData(): ?OrderMetaData
    {
        return $this->metaData;
    }
}
