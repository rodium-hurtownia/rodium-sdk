<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\SerializerInterface;
use Psr\Http\Client\ClientInterface;
use Rodium\Sdk\Client\AbstractApi;
use Rodium\Sdk\Client\ResourceNotFoundException;
use Rodium\Sdk\Page\Page;
use Rodium\Sdk\Page\PageRequest;

final class OrdersHttpApi extends AbstractApi implements OrdersApi
{
    private const BRANCH_ID_HEADER = 'X-Rodium-BRANCH-ID';

    /** @var int */
    private $branchId;

    public function __construct(ClientInterface $client, SerializerInterface $serializer, ?int $branchId = null)
    {
        parent::__construct($client, $serializer);
        $this->branchId = $branchId;
    }

    /**
     * @inheritDoc
     */
    public function orderOfId(string $id): ?Order
    {
        try {
            return $this->get(sprintf("/orders/%s", $id), Order::class);
        } catch (ResourceNotFoundException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function orders(?PageRequest $pageRequest = null): Page
    {
        $pageRequest = $pageRequest ?? PageRequest::defaultPageRequest();
        return $this->get(
            sprintf("/orders?%s", $pageRequest->toQueryString()),
            sprintf("%s<%s>", Page::class, Order::class)
        );
    }

    public function place(Order $order): Order
    {
        return $this->post("/orders", $order, Order::class);
    }

    /**
     * @inheritDoc
     */
    protected function defaultHeaders(): array
    {
        if ($this->branchId) {
            return [self::BRANCH_ID_HEADER => $this->branchId];
        }

        return [];
    }
}
