<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\Annotation as JMS;

final class OrderTotals
{
    /**
     * The net value
     *
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("net")
     */
    private $net;

    /**
     * The net value after discount
     *
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("net_discount")
     */
    private $netDiscount;

    /**
     * The gross value
     *
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("gross")
     */
    private $gross;

    /**
     * The gross value after discount
     *
     * @var float
     * @JMS\Type("float")
     * @JMS\SerializedName("gross_discount")
     */
    private $grossDiscount;

    public function __construct(?float $net, ?float $netDiscount, ?float $gross, ?float $grossDiscount)
    {
        $this->net = $net;
        $this->netDiscount = $netDiscount;
        $this->gross = $gross;
        $this->grossDiscount = $grossDiscount;
    }

    public function net(): ?float
    {
        return $this->net;
    }

    public function netDiscount(): ?float
    {
        return $this->netDiscount;
    }

    public function gross(): ?float
    {
        return $this->gross;
    }

    public function grossDiscount(): ?float
    {
        return $this->grossDiscount;
    }
}