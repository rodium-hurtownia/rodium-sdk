<?php

namespace Rodium\Sdk\Order;

use Rodium\Sdk\Client\ResourceNotFoundException;
use Rodium\Sdk\Page\Page;
use Rodium\Sdk\Page\PageRequest;

interface OrdersApi
{
    /**
     * Gets the order by its ID
     *
     * @param string $id the ID of the order
     * @return Order|null the requested order
     */
    public function orderOfId(string $id): ?Order;

    /**
     * Gets the paginated order list
     *
     * @param PageRequest|null $pageRequest the page number and size
     * @return Page the requested page of orders
     * @throws ResourceNotFoundException if page does not exist
     */
    public function orders(?PageRequest $pageRequest = null): Page;

    /**
     * Places a new order
     *
     * @param Order $order the order to be places
     * @return Order the placed order
     */
    public function place(Order $order): Order;
}