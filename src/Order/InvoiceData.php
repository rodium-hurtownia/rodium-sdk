<?php

namespace Rodium\Sdk\Order;

use JMS\Serializer\Annotation as JMS;

final class InvoiceData
{
    /**
     * The name
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     */
    private $name;

    /**
     * The VAT Number
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("vat_no")
     */
    private $vatNo;

    /**
     * The street address
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("street_and_number")
     */
    private $streetAndNumber;

    /**
     * The post code
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("post_code")
     */
    private $postCode;

    /**
     * The post
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("post")
     */
    private $post;

    /**
     * The province
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("province")
     */
    private $province;

    /**
     * The country
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("country")
     */
    private $country;

    /**
     * The email address
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("email")
     */
    private $email;

    /**
     * The phone number
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("phone_no")
     */
    private $phoneNo;

    public function __construct(
        ?string $name,
        ?string $vatNo,
        ?string $streetAndNumber,
        ?string $postCode,
        ?string $post,
        ?string $province = null,
        ?string $country = null,
        ?string $email = null,
        ?string $phoneNo = null
    ) {
        $this->name = $name;
        $this->vatNo = $vatNo;
        $this->streetAndNumber = $streetAndNumber;
        $this->postCode = $postCode;
        $this->post = $post;
        $this->province = $province;
        $this->country = $country;
        $this->email = $email;
        $this->phoneNo = $phoneNo;
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function vatNo(): ?string
    {
        return $this->vatNo;
    }

    public function streetAndNumber(): ?string
    {
        return $this->streetAndNumber;
    }

    public function postCode(): ?string
    {
        return $this->postCode;
    }

    public function post(): ?string
    {
        return $this->post;
    }

    public function province(): ?string
    {
        return $this->province;
    }

    public function country(): ?string
    {
        return $this->country;
    }

    public function email(): ?string
    {
        return $this->email;
    }

    public function phoneNo(): ?string
    {
        return $this->phoneNo;
    }
}