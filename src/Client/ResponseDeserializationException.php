<?php

namespace Rodium\Sdk\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class ResponseDeserializationException extends AbstractApiException
{
    public function __construct(
        RequestInterface $request,
        ResponseInterface $response = null,
        \Throwable $previous = null
    ) {
        parent::__construct(
            "The response could not be deserialized to the request type. Very likely the message has come in the unexpected format.",
            $request,
            $response,
            $previous
        );
    }
}