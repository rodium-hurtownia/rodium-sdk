<?php

namespace Rodium\Sdk\Client;

final class ApiKey
{
    public const HEADER = "X-Rodium-API-KEY";

    /** @var string */
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function __toString(): string
    {
        return $this->apiKey;
    }
}