<?php

namespace Rodium\Sdk\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rodium\Sdk\Error\AbstractError;

final class ResourceNotFoundException extends AbstractApiException
{
    public function __construct(
        RequestInterface $request,
        ?ResponseInterface $response = null,
        ?\Throwable $previous = null,
        ?AbstractError $error = null
    ) {
        parent::__construct(
            sprintf("The requested resource \"%s\" could not be found.", $request->getUri()),
            $request,
            $response,
            $previous,
            $error
        );
    }

    public static function create(
        RequestInterface $request,
        ?ResponseInterface $response = null,
        ?\Throwable $previous = null,
        ?AbstractError $error = null
    ): self {
        return new self($request, $response, $previous, $error);
    }
}