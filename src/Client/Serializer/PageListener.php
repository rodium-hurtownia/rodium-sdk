<?php

namespace Rodium\Sdk\Client\Serializer;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\EventDispatcher\PreDeserializeEvent;
use Rodium\Sdk\Page\Page;

final class PageListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => 'serializer.pre_deserialize',
                'method' => 'onPreDeserialize',
                'class' => Page::class,
                'format' => 'json',
                'priority' => 0
            ],
            [
                'event' => 'serializer.post_deserialize',
                'method' => 'onPostDeserialize',
                'class' => Page::class,
                'format' => 'json',
                'priority' => 0
            ]
        ];
    }

    /**
     * Swaps the elements deserialization meta data type
     *
     * @param PreDeserializeEvent $event
     */
    public function onPreDeserialize(PreDeserializeEvent $event)
    {
        $pageType = $event->getType();
        $innerPageType = $this->resolveInnerPageType($pageType);
        if (!$innerPageType) {
            return;
        }

        $metaData = $event->getContext()->getMetadataFactory()->getMetadataForClass(Page::class);
        $type = $metaData->propertyMetadata['elements']->type;
        $type['params'] = [['name' => $innerPageType]];
        $metaData->propertyMetadata['elements']->type = $type;
    }

    /**
     * Reverts the elements deserialization meta data type
     *
     * @param ObjectEvent $event
     */
    public function onPostDeserialize(ObjectEvent $event)
    {
        $metaData = $event->getContext()->getMetadataFactory()->getMetadataForClass(Page::class);
        $type = $metaData->propertyMetadata['elements']->type;
        $type['params'] = [];
        $metaData->propertyMetadata['elements']->type = $type;
    }

    private function resolveInnerPageType(array $type): ?string
    {
        if (empty($type['params']) || empty($type['params'][0]) || empty($type['params'][0]['name'])) {
            return null;
        }

        return (string)$type['params'][0]['name'];
    }
}