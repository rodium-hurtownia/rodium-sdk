<?php

namespace Rodium\Sdk\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rodium\Sdk\Error\AbstractError;

final class GenericApiException extends AbstractApiException
{
    public static function unexpectedError(
        ?RequestInterface $request = null,
        ?ResponseInterface $response = null,
        ?\Throwable $previous = null,
        ?AbstractError $error = null
    ): self
    {
        return new self(
            "Unexpected error has occurred",
            $request,
            $response,
            $previous,
            $error
        );
    }
}