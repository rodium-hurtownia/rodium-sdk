<?php

namespace Rodium\Sdk\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rodium\Sdk\Error\AbstractError;

abstract class AbstractApiException extends \RuntimeException
{
    /** @var RequestInterface */
    private $request;

    /** @var ResponseInterface */
    private $response;

    /** @var AbstractError */
    private $error;

    public function __construct(
        $message,
        ?RequestInterface $request = null,
        ?ResponseInterface $response = null,
        \Throwable $previous = null,
        ?AbstractError $error = null
    ) {
        $this->request = $request;
        $this->response = $response;
        $this->error = $error;
        parent::__construct($message, null, $previous);
    }

    public function request(): ?RequestInterface
    {
        return $this->request;
    }

    public function response(): ?ResponseInterface
    {
        return $this->response;
    }

    public function error(): AbstractError
    {
        return $this->error;
    }
}