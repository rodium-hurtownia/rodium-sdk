<?php

namespace Rodium\Sdk\Client;

use Buzz\Browser;
use Buzz\Exception\ClientException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rodium\Sdk\Error\AbstractError;
use Rodium\Sdk\Error\GenericError;
use Rodium\Sdk\Error\ValidationError;

/**
 * The base class for all specific APIs implementations
 */
abstract class AbstractApi
{
    /** @var Browser */
    private $client;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(
        ClientInterface $client,
        SerializerInterface $serializer
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * Wraps GET request to given path and deserializes the response to given data type
     *
     * @param string $path the path of the GET request
     * @param string $dataType the data type the response should be deserialized to
     * @param array $headers the additional headers to be passed to the request
     * @return mixed the deserialized response
     * @throws GenericApiException
     * @throws ResponseDeserializationException
     */
    protected function get(string $path, string $dataType, array $headers = [])
    {
        try {
            $response = $this->client->get($path, $this->headers($headers));
        } catch (ClientException $e) {
            throw GenericApiException::unexpectedError($this->client->getLastRequest(), null, $e);
        }

        return $this->handleResponse($response, $dataType);
    }

    /**
     * Wraps POST request to given path and deserializes the response to given data type
     *
     * @param string $path the path of the POST request
     * @param RequestData|mixed $data the data to be sent in the request body
     * @param ?string $dataType the data type the response should be deserialized to
     * @param array $headers the additional headers to be passed to the request
     * @return mixed the deserialized response
     * @throws GenericApiException
     * @throws ResponseDeserializationException
     */
    protected function post(string $path, $data, ?string $dataType = null, array $headers = [])
    {
        try {
            $data = $data === null ? RequestData::empty() : $data instanceof RequestData ? $data : RequestData::withoutGroups($data);
            $response = $this->client->post(
                $path,
                $this->headers($headers),
                !$data->isEmpty() ? $this->serializer->serialize($data->data(), "json", $data->groups() ? SerializationContext::create()->setGroups($data->groups()) : null) : ""
            );

        } catch (ClientException $e) {
            throw GenericApiException::unexpectedError($this->client->getLastRequest(), null, $e);
        }

        return $this->handleResponse($response, $dataType);
    }

    /**
     * Wraps PUT request to given path and deserializes the response to given data type
     *
     * @param string $path the path of the PUT request
     * @param RequestData|mixed $data the data to be sent in the request body
     * @param ?string $dataType the data type the response should be deserialized to
     * @param array $headers the additional headers to be passed to the request
     * @return mixed the deserialized response
     * @throws GenericApiException
     * @throws ResponseDeserializationException
     */
    protected function put(string $path, $data, ?string $dataType = null, array $headers = [])
    {
        try {
            $data = $data === null ? RequestData::empty() : $data instanceof RequestData ? $data : RequestData::withoutGroups($data);

            $response = $this->client->put(
                $path,
                $this->headers($headers),
                !$data->isEmpty() ? $this->serializer->serialize($data->data(), "json", $data->groups() ? SerializationContext::create()->setGroups($data->groups()) : null) : ""
            );
        } catch (ClientException $e) {
            throw GenericApiException::unexpectedError($this->client->getLastRequest(), null, $e);
        }

        return $this->handleResponse($response, $dataType);
    }

    /**
     * Wraps DELETE request to given path and deserializes the response to given data type
     *
     * @param string $path the path of the DELETE request
     * @param ?string $dataType the data type the response should be deserialized to
     * @param array $headers the additional headers to be passed to the request
     * @return mixed the deserialized response
     * @throws GenericApiException
     * @throws ResponseDeserializationException
     */
    protected function delete(string $path, ?string $dataType = null, array $headers = [])
    {
        try {
            $response = $this->client->delete(
                $path,
                $this->headers($headers)
            );
        } catch (ClientException $e) {
            throw GenericApiException::unexpectedError($this->client->getLastRequest(), null, $e);
        }

        return $this->handleResponse($response, $dataType);
    }

    /**
     * Handles the response from the HTTP Client. Checks the status code and deserializes the content.
     *
     * @param ResponseInterface $response the response from the HTTP Client
     * @param ?string $dataType the data type the content should be deserialized to
     * @return mixed deserialized content
     */
    private function handleResponse(ResponseInterface $response, ?string $dataType)
    {
        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            throw $this->exception($this->client->getLastRequest(), $response);
        }

        if (!$dataType) {
            return $response->getBody()->getContents();
        }

        return $this->deserialize($response, $dataType);
    }

    private function deserialize(ResponseInterface $response, string $dataType)
    {
        try {
            return $this->serializer->deserialize($response->getBody()->getContents(), $dataType, "json");
        } catch (RuntimeException $e) {
            throw new ResponseDeserializationException($this->client->getLastRequest(), $response, $e);
        }
    }

    /**
     * Creates the exception for given request and response objects
     */
    private function exception(RequestInterface $request, ?ResponseInterface $response = null): AbstractApiException
    {
        if (!$response) {
            throw new GenericApiException("Unexpected error has occurred", $request);
        }

        $apiError = $this->toApiError($response);
        switch ($response->getStatusCode()) {
            case 404:
                return ResourceNotFoundException::create($request, $response, null, $apiError);
            default:
                return GenericApiException::unexpectedError($request, $response, null, $apiError);
        }
    }

    /**
     * Returns default headers to be set with every request.
     * The method to be overridden in the concrete API
     *
     * @return array the default headers
     */
    protected function defaultHeaders(): array
    {
        return [];
    }

    protected function headers(array $headers): array
    {
        return array_replace($this->defaultHeaders(), $headers);
    }

    private function toApiError(ResponseInterface $response): ?AbstractError
    {
        switch ($response->getStatusCode()) {
            case 422:
                $dataType = ValidationError::class;
                break;
            default:
                $dataType = GenericError::class;
        }

        try {
            return $this->deserialize($response, $dataType);
        } catch (ResponseDeserializationException $e) {
            return null;
        }
    }
}
