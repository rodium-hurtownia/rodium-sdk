<?php

namespace Rodium\Sdk\Client\Buzz;

use Buzz\Middleware\MiddlewareInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Rodium\Sdk\Client\ApiKey;

final class ApiKeyAuthMiddleware implements MiddlewareInterface
{
    /** @var ApiKey */
    private $apiKey;

    public function __construct(ApiKey $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function handleRequest(RequestInterface $request, callable $next)
    {
        return $next($request->withHeader(ApiKey::HEADER, (string)$this->apiKey));
    }

    public function handleResponse(RequestInterface $request, ResponseInterface $response, callable $next)
    {
        return $next($request, $response);
    }
}
