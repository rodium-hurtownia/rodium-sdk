<?php

namespace Rodium\Sdk\Client\Buzz;

use Buzz\Middleware\MiddlewareInterface;
use Nyholm\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class BaseUrlMiddleware implements MiddlewareInterface
{
    /** @var string */
    private $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = rtrim($baseUrl, '/');
    }

    public function handleRequest(RequestInterface $request, callable $next)
    {
        return $next($request->withUri(new Uri($this->baseUrl . $request->getUri())));
    }

    public function handleResponse(RequestInterface $request, ResponseInterface $response, callable $next)
    {
        return $next($request, $response);
    }
}