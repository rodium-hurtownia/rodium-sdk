<?php

namespace Rodium\Sdk\Client;

/**
 * Represents the data to be serialized into request body
 * @internal for internal use only
 */
final class RequestData
{
    /** @var mixed */
    private $data;

    /** @var array */
    private $groups;

    private function __construct($data, array $groups = [])
    {
        $this->data = $data;
        $this->groups = $groups;
    }

    public static function withoutGroups($data): self
    {
        return new self($data);
    }

    public static function withGroups($data, array $groups): self
    {
        return new self($data, $groups);
    }

    public static function empty(): self
    {
        return self::withoutGroups(null);
    }

    public function data()
    {
        return $this->data;
    }

    public function groups(): array
    {
        return $this->groups;
    }

    public function isEmpty(): bool
    {
        return !$this->data;
    }
}