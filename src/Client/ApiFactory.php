<?php

namespace Rodium\Sdk\Client;

use Buzz\Browser;
use Buzz\Client\Curl;
use JMS\Serializer\EventDispatcher\EventDispatcherInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Nyholm\Psr7\Factory\Psr17Factory;
use Rodium\Sdk\Catalog\Categories\SupplierCategoriesApi;
use Rodium\Sdk\Catalog\Categories\SupplierCategoriesHttpApi;
use Rodium\Sdk\Catalog\Items\SupplierItemsApi;
use Rodium\Sdk\Catalog\Items\SupplierItemsHttpApi;
use Rodium\Sdk\Client\Buzz\ApiKeyAuthMiddleware;
use Rodium\Sdk\Client\Buzz\BaseUrlMiddleware;
use Rodium\Sdk\Catalog\Categories\CategoriesApi;
use Rodium\Sdk\Catalog\Categories\CategoriesHttpApi;
use Rodium\Sdk\Catalog\Items\ItemsApi;
use Rodium\Sdk\Catalog\Items\ItemsHttpApi;
use Rodium\Sdk\Client\Serializer\PageListener;
use Rodium\Sdk\Order\OrdersApi;
use Rodium\Sdk\Order\OrdersHttpApi;

final class ApiFactory
{
    /** @var Browser */
    private $client;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(string $baseUrl, ApiKey $apiKey, array $curlOptions = [])
    {
        $psr17Factory = new Psr17Factory();
        $this->client = new Browser(new Curl($psr17Factory, $curlOptions), $psr17Factory);
        $this->client->addMiddleware(new BaseUrlMiddleware($baseUrl));
        $this->client->addMiddleware(new ApiKeyAuthMiddleware($apiKey));

        $this->serializer = SerializerBuilder::create()
            ->configureListeners(function (EventDispatcherInterface $eventDispatcher) {
                $eventDispatcher->addSubscriber(new PageListener());
            })
        ->build();
    }

    /**
     * Creates the CategoriesApi
     *
     * @return CategoriesApi
     */
    public function categoriesApi(): CategoriesApi
    {
        return new CategoriesHttpApi(
            $this->client,
            $this->serializer
        );
    }

    /**
     * Creates the SupplierCategoriesApi
     *
     * @return SupplierCategoriesApi
     */
    public function supplierCategoriesApi(): SupplierCategoriesApi
    {
        return new SupplierCategoriesHttpApi(
            $this->client,
            $this->serializer
        );
    }

    /**
     * Creates the ItemsApi
     *
     * @return ItemsApi
     */
    public function itemsApi(): ItemsApi
    {
        return new ItemsHttpApi(
            $this->client,
            $this->serializer
        );
    }

    /**
     * Creates the SupplierItemsApi
     *
     * @return SupplierItemsApi
     */
    public function supplierItemsApi(): SupplierItemsApi
    {
        return new SupplierItemsHttpApi(
            $this->client,
            $this->serializer
        );
    }

    /**
     * Creates the OrdersApi
     *
     * @param ?int $branchId the identifier of the branch (required only if customer has more than one branch)
     * @return OrdersApi
     */
    public function ordersApi(?int $branchId = null): OrdersApi
    {
        return new OrdersHttpApi(
            $this->client,
            $this->serializer,
            $branchId
        );
    }
}
