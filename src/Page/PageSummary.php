<?php

namespace Rodium\Sdk\Page;

use JMS\Serializer\Annotation as JMS;

final class PageSummary
{
    /**
     * The PageRequest
     *
     * @var PageRequest
     * @JMS\Type("Rodium\Sdk\Page\PageRequest")
     * @JMS\SerializedName("page_request")
     */
    private $pageRequest;

    /**
     * The total number of Pages
     *
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("total_pages")
     */
    private $totalPages;

    /**
     * The total number of elements
     *
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("total_elements")
     */
    private $totalElements;

    public function __construct(PageRequest $pageRequest, int $totalPages, int $totalElements)
    {
        $this->pageRequest = $pageRequest;
        $this->totalPages = $totalPages;
        $this->totalElements = $totalElements;
    }

    public function pageRequest(): PageRequest
    {
        return $this->pageRequest;
    }

    public function totalPages(): int
    {
        return $this->totalPages;
    }

    public function totalElements(): int
    {
        return $this->totalElements;
    }

    /**
     * Generates the PageRequest for the next Page (returns null if there is no more Pages)
     *
     * @return ?PageRequest
     */
    public function nextPage(): ?PageRequest
    {
        $pageRequest = $this->pageRequest->nextPage();
        if ($pageRequest->page() <= $this->totalPages) {
            return $pageRequest;
        }

        return null;
    }
}