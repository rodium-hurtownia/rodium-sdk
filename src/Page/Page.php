<?php

namespace Rodium\Sdk\Page;

use JMS\Serializer\Annotation as JMS;

/**
 * Represents the Page of elements
 */
final class Page implements \IteratorAggregate, \Countable
{
    /**
     * The elements
     *
     * @var array
     * @JMS\SerializedName("elements")
     * @JMS\Type("array")
     */
    private $elements;

    /**
     * The Page summary
     *
     * @var PageSummary
     * @JMS\Type("Rodium\Sdk\Page\PageSummary")
     * @JMS\SerializedName("summary")
     */
    private $summary;

    public function __construct(array $elements, PageSummary $summary)
    {
        $this->elements = $elements;
        $this->summary = $summary;
    }

    /**
     * Gets the page elements
     *
     * @return array
     */
    public function elements(): array
    {
        return $this->elements;
    }

    /**
     * Gets the page summary
     *
     * @return PageSummary
     */
    public function summary(): PageSummary
    {
        return $this->summary;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->elements);
    }
}