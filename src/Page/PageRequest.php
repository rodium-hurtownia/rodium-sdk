<?php

namespace Rodium\Sdk\Page;

use JMS\Serializer\Annotation as JMS;

/**
 * Represents the requested page
 */
final class PageRequest
{
    /**
     * The requested page number
     *
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("page")
     */
    private $page;

    /**
     * The requested number of elements per page
     *
     * @var int
     * @JMS\Type("int")
     * @JMS\SerializedName("page_size")
     */
    private $pageSize;

    public function __construct(int $page, int $pageSize)
    {
        $this->page = $page;
        $this->pageSize = $pageSize;
    }

    public static function pageRequest(int $page = 1, int $pageSize = 25): self
    {
        return new self($page, $pageSize);
    }

    public function page(): int
    {
        return $this->page;
    }

    public function pageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * Creates a default PageRequest
     *
     * @return PageRequest
     */
    public static function defaultPageRequest(): self
    {
        return new self(1, 25);
    }

    /**
     * Creates the request for the next Page
     *
     * @return PageRequest
     */
    public function nextPage(): self
    {
        return new self($this->pageSize, $this->page + 1);
    }

    /**
     * Creates the query string from this PageRequest
     *
     * @return string
     */
    public function toQueryString(): string
    {
        return sprintf('page=%d&size=%d', $this->page, $this->pageSize);
    }

    public function __toString(): string
    {
        return $this->toQueryString();
    }
}
