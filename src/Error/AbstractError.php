<?php

namespace Rodium\Sdk\Error;

use JMS\Serializer\Annotation as JMS;

abstract class AbstractError
{
    /**
     * The error message
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("message")
     */
    private $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    public function message(): string
    {
        return $this->message;
    }
}
