<?php

namespace Rodium\Sdk\Error;

use JMS\Serializer\Annotation as JMS;

final class ValidationError extends AbstractError
{
    /**
     * The list of violations
     *
     * @var array
     * @JMS\Type("array")
     * @JMS\SerializedName("violations")
     */
    private $violations;

    public function __construct(string $message, array $violations)
    {
        parent::__construct($message);
        $this->violations = $violations;
    }

    public function violations(): array
    {
        return $this->violations;
    }
}
